FROM ruby:alpine

## File imagemagick  is need for paperclip
RUN apk --update add --virtual build-dependencies build-base git ruby-dev openssl-dev libxml2-dev libxslt-dev \
   libc-dev linux-headers nodejs tzdata file imagemagick && gem install bundler
RUN gem update bundler
RUN mkdir /app
VOLUME ["/app"]
WORKDIR /app
# RUN gem source --add http://ideractive.com:9292

#FROM ideractive/expo:latest

#RUN gem install idr_active_extension -v "~> 0.1"
#RUN gem install idr_media_manager -v "~> 0.1"

ADD Gemfile /app/Gemfile
ADD Gemfile.lock /app/Gemfile.lock

COPY ./gems/idr_active_extension-0.1.4.gem /app/idr_active_extension-0.1.4.gem
RUN gem install --local idr_active_extension-0.1.4.gem

COPY ./gems/idr_media_manager-0.1.5.gem /app/idr_media_manager-0.1.5.gem
RUN gem install --local idr_media_manager-0.1.5.gem

COPY ./gems/idr_renderer-0.1.4.gem /app/idr_renderer-0.1.4.gem
RUN gem install --local idr_renderer-0.1.4.gem

RUN bundle install

# Set Rails to run in production
ENV SECRET_KEY_BASE aa89a3fe86780a087166420560f0951ef777ed2450922b05139a70b04143e59d4ab5623c16b1859464e76c0d736093767ba7433484d2759452851aeaa154dfbe
ENV RAILS_ENV production
ENV RACK_ENV production

# Precompile Rails assets
# RUN bundle exec rake assets:precompile

# Start puma
# CMD bundle exec puma -C config/puma.rb
