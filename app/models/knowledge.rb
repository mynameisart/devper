class Knowledge

  include Mongoid::Document

  field :title,             type: String, default: ""
  field :reflink,              type: String, default: ""
  field :body,              type: String, default: ""
  include Mongoid::Timestamps

  has_many :issues

end
