class Componant
  include Mongoid::Document

  field :title,                         type: String,  default: ""
  field :description,                   type: String,  default: ""
  include Mongoid::Timestamps

  belongs_to :project
  has_many :issues, inverse_of: :milestone

  def to_s
    "#{title}"
  end

  def display_name
    "#{title}"
  end

end
