class Issue
  include Mongoid::Document
  include Mongoid::Orderable

  TYPES = ['story', 'bug', 'discussion']
  STATUS = ['queue', 'doing', 'testing', 'complete']
  PRIORITY = ['major', 'minor', 'criticle']

  field :type,                          type: String,  default: "" # story, bug, discussion
  field :priority,                      type: String,  default: "" # 0-5
  field :position,                      type: Integer, default: "" # 0-5
  field :title,                         type: String,  default: ""
  field :description,                   type: String,  default: ""
  field :due_date,                      type: Date,    default: ""
  field :estimate_manhours,             type: Float,   default: 0
  field :status,                        type: String,  default: "queue" # queue, doing, testing, complete
  field :test_script,                   type: String,  default: ""
  field :robot_test_script,             type: String,  default: ""
  include Mongoid::Timestamps

  belongs_to :user
  belongs_to :project
  belongs_to :milestone
  belongs_to :componant

  embeds_many :attachments,
              :cascade_callbacks => true,
              class_name:"Attachment",  # Class name
              store_as: 'attachments'  # Field name

  accepts_nested_attributes_for :attachments, :allow_destroy => true

  orderable column: :position

  validates :type, presence: true, allow_blank: false
  validates :title, presence: true, allow_blank: false
  validates :status, presence: true, allow_blank: false
  validates :estimate_manhours, presence: true, allow_blank: false, :numericality => { :greater_than_or_equal_to => 0 }

  after_save :update_project_time

  def display_name
    "[#{project.name}] #{title}"
  end
  def is_over_due
    require 'date'
    self.status != 'complete' and self.status != 'testing' and !self.due_date.nil? and self.due_date < Date.today
  end
  def label_status
    if self.is_over_due
      return 'warning ' + self.status
    else
      return self.status
    end
  end

  def update_project_time
      self.project.projection_manhours = self.project.issues.sum(&:estimate_manhours)
      # self.project.save()
  end
end
