class Milestone
  include Mongoid::Document

  field :title,                         type: String,  default: ""
  field :description,                   type: String,  default: ""
  field :due_date,                      type: Date,    default: ""
  include Mongoid::Timestamps

  belongs_to :project
  has_many :issues, inverse_of: :milestone

end
