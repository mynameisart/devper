class Project
  include Mongoid::Document

  field :name,           type: String, default: ""
  field :project_code,           type: String, default: ""
  field :description,           type: String, default: ""
  field :git,           type: String, default: ""
  field :trello,           type: String, default: ""
  field :estimate_manhours,           type: Float, default: 0
  field :projection_manhours,           type: Float, default: 0
  field :actual_manhours,           type: Float, default: 0
  field :issue_count,           type: Float, default: 0
  field :story_count,           type: Float, default: 0
  field :complete_story_count,           type: Float, default: 0
  field :bug_count,           type: Float, default: 0
  field :complete_bug_count,           type: Float, default: 0
  field :complete_issue_count,           type: Float, default: 0
  include Mongoid::Timestamps

  has_and_belongs_to_many :users, inverse_of: :projects
  has_many :timesheets, inverse_of: :project
  has_many :issues, inverse_of: :project
  has_many :milestones, inverse_of: :project

  accepts_nested_attributes_for :milestones, :allow_destroy => true
  accepts_nested_attributes_for :issues, :allow_destroy => true
  accepts_nested_attributes_for :users, allow_destroy: true

  validates :name, presence: true, allow_blank: false
  validates :project_code, presence: true, allow_blank: false
  validates :estimate_manhours, presence: true, allow_blank: false, :numericality => { :greater_than_or_equal_to => 0 }
end
