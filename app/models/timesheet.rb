class Timesheet
  include Mongoid::Document

  @@type_list = ['story', 'bug', 'discussion']
  def self.type_list
    @@type_list
  end
  @@status_list = ['queue', 'doing', 'testing', 'complete']
  def self.status_list
    @@status_list
  end

  # field :title,                         type: String, default: ""
  field :description,                   type: String, default: ""
  field :work_date,                     type: Date, default:  Time.now
  field :work_hours,                    type: Float,  default: 0
  include Mongoid::Timestamps

  belongs_to :project
  belongs_to :issue
  belongs_to :user

  validates :project, presence: true, allow_blank: false
  validates :issue, presence: true, allow_blank: false
  validates :work_hours, presence: true, allow_blank: false
  validates :work_date, presence: true, allow_blank: false

  after_save :after_save_callback
  def after_save_callback
      self.project.actual_manhours = self.project.timesheets.sum(&:work_hours)
      self.project.save()
  end
end
