class Leaveform
  include Mongoid::Document

  TYPES = ['sick', 'personal', 'vacation']
  STATUS = ['allow', 'not_allow']

  field :name,                   type: String,  default: ""
  field :type,                   type: String,  default: "" # sick, personal, vacation
  field :title,                  type: String,  default: ""
  field :starts_at,              type: Date,    default: ""
  field :ends_at,                type: Date,    default: ""
  field :status,                 type: String,  default: "not_allow" # allow, not allow
  include Mongoid::Timestamps


  belongs_to :user

  # validates :name, presence: true, allow_blank: false
  validates :type, presence: true, allow_blank: false
  validates :starts_at, presence: true, allow_blank: false
  validates :ends_at, presence: true, allow_blank: false

end
