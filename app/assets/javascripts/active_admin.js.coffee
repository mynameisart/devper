#= require active_admin/base
#= require activeadmin-sortable
#= require active_admin/select2
#= require ./dependent_select
#= require moment-with-locales
#= require full-calendar/fullcalendar

$(document).ready ->
    if($('.input-markdown').length > 0)
        simplemde = new SimpleMDE(element: $('.input-markdown textarea')[0])
