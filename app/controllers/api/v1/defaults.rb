module API
  module V1
    module Defaults
      # if you're using Grape outside of Rails, you'll have to use Module#included hook
      extend ActiveSupport::Concern

      included do
        # common Grape settings
        prefix "api"
        version "v1", using: :path
        default_format :json
        format :json
        formatter :json,
                  Grape::Formatter::ActiveModelSerializers

        helpers do
          def permitted_params
            @permitted_params ||= declared(params, include_missing: false)
          end

          def logger
            Rails.logger
          end

          def current_ip_address
            env['HTTP_X_FORWARDED_FOR'] || env['REMOTE_ADDR']
          end

          def current_user_agent
            env['HTTP_USER_AGENT']
          end
        end

        # rescue_from ActiveRecord::RecordNotFound do |e|
        #   error_response(message: e.message, status: 404)
        # end
        #
        # rescue_from ActiveRecord::RecordInvalid do |e|
        #   error_response(message: e.message, status: 422)
        # end

        # # global handler for simple not found case
        # rescue_from ActiveRecord::RecordNotFound do |e|
        #   error_response(message: e.message, status: 404)
        # end

        # global exception handler, used for error notifications
        # rescue_from :all do |e|
        #   if Rails.env.development?
        #     raise e
        #   else
        #     Raven.capture_exception(e)
        #     error_response(message: "Internal server error", status: 500)
        #   end
        # end

        # HTTP header based authentication
        # before do
        #   error!('Unauthorized', 401) unless headers['Authorization'] == "some token"
        # end
      end
    end
  end
end