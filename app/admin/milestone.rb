ActiveAdmin.register Milestone do

  menu parent: "Projects"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  permit_params do
    permitted = [:title, :description, :duedate, :project_id]
    # permitted << :other if resource.something?
    permitted
  end

  collection_action :ajax_filter, method: :get do
    # Do some CSV importing work here...

    @items = Componant

    if params[:project_id]
      resources = @items.where('project_id' => params[:project_id])
      @items = resources
    end

    if @items == Componant
      render :json => []
    else
      render :json => @items.collect {|item| {:id => item.id.to_s, :name => item.display_name} }
    end
  end

  index do
    selectable_column
    column :title
    column :description
    column :duedate
    column :project

    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs "Admin Details" do
      f.input :title
      f.input :description
      f.input :duedate
      f.input :project
    end

    f.actions
  end

  # action_item :create_timesheet do
  #
  #   link_to 'Create Timesheet', new_admin_timesheet_path(:timesheet => { :project_id => project })
  # end


end