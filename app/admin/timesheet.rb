ActiveAdmin.register Timesheet do

  menu parent: "Projects"

  # scope_to :current_user

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  permit_params do
    permitted = [:title, :description, :work_date, :work_hours,  :project_id, :issue_id, :user_id]
    # permitted << :other if resource.something?
    permitted
  end

  config.remove_action_item(:new)

  before_save do |instance|
      if instance.user.nil?
          instance.user = current_user
      end
  end

  filter :project, :as => :select, :collection =>  proc{ current_user.projects }

  index do
    selectable_column
    column :project
    column :issue
    column :description
    column :due_date
    column :user
    column :updated_at

    actions default: true do |resource|
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    # Get current project or pick first one if not exist
    project = nil
    if !params[:timesheet].nil?
      if !params[:timesheet][:project_id].nil?
        project = Project.find(params[:timesheet][:project_id])
      end
      if !params[:timesheet][:issue_id].nil?
        issue = Issue.find(params[:timesheet][:issue_id])
        project = issue.project
      end
    end
    project ||= Project.first

    f.inputs "Admin Details" do
      f.input :project, :as => :select2, include_blank: false
      #
      # if current_user.role == 'superadmin'
      #     f.input :user, include_blank: false
      # end

      # f.input :title
      f.input :issue, :as => :select2, include_blank: false, :input_html => {'data-option-dependent' => true, 'data-option-url' => ajax_filter_admin_issues_path(:format => 'json') + '?project_id=:timesheet_project_id&status=doing', 'data-option-observed' => 'timesheet_project_id'}, :collection => []

      f.input :description
      f.input :work_date, as: :datepicker, datepicker_options: { min_date: 5.days.ago.to_date, max_date: "+1d" }
      f.input :work_hours
    end
    f.actions
  end

  action_item :new, only: :index do
    # Get current project or pick first one if not exist
    if !params[:timesheet].nil?
      if !params[:timesheet][:project_id].nil?
        project = Project.find(params[:timesheet][:project_id])
      end
      if !params[:timesheet][:issue_id].nil?
        issue = Issue.find(params[:timesheet][:issue_id])
        project = issue.project
      end
    end
    project ||= Project.first

    # Link to create page
    link_to 'Create Timesheet', new_admin_timesheet_path(:timesheet => {:project_id => project})
  end


end
