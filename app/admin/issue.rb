# class ::ActiveAdmin::Views::IndexAsIssueIndex < ActiveAdmin::Views::IndexAsTable
#
#   def build(page_presenter, collection)
#     @item = Issue.new()
#
#     #or put _foo.html.erb to views/admin/admin_users
#     render :partial=>'quick_add', :layout=>false
#     super
#   end
# end
#

ActiveAdmin.register Issue do
  menu parent: "Projects"

  # scope :status

  permit_params do
    permitted = [:type, :title, :description, :due_date, :estimate_manhours, :status, :priority, :project_id, :user_id, :componant_id, attachments_attributes: [:attachment,:id, :_destroy]]
    # permitted << :other if resource.something?
    permitted
  end

  # before_save do |instance|
  #     if instance.user.nil?
  #         instance.user = current_user
  #     end
  # end

  config.sort_order = 'position_asc' # assumes you are using 'position' for your acts_as_list column
  # config.paginate   = false # optional; drag-and-drop across pages is not supported

  # sortable # creates the controller action which handles the sorting

  #You can set the :collection via a proc which will be called within the view
  filter :title
  filter :type, as: :select, collection: Issue::TYPES
  filter :project, :as => :select, :collection =>  proc{ current_user.projects }
  filter :status, as: :select, collection: Issue::STATUS
  filter :due_date
  # filter :componant, as: :select, collection: Compo::TYPES

  scope :in_progress do |scope|
    scope.where(:status => {'$nin' => ['testing', 'complete']})
  end
  scope("Overdue") { |scope| scope.where(:status => {'$nin' => ['testing', 'complete']}, :due_date => { '$lt' => Date.today}) }
  scope("Testing") { |scope| scope.where(:status =>  'testing') }
  scope("Complete") { |scope| scope.where(:status =>  'complete') }
  scope :all, default: true


  collection_action :ajax_filter, method: :get do

    @items = Issue

    if params[:project_id]
      resources = @items.where('project_id' => params[:project_id])
      @items = resources
    end

    if params[:status]
      resources = @items.where('status' => params[:status])
      @items = resources
    end

    if params[:due_date]
      if params[:due_date] == 'false'
        resources = @items.where('due_date' => nil)
      else
        resources = @items.where('due_date' => params[:due_date])
      end
      @items = resources
    end

    if @items == Issue
      render :json =>[]
    else
      render :json => @items.collect {|item| {:id => item.id.to_s, :name => item.display_name} }
    end
  end

  collection_action :quick_create, method: :get do
    @item = Issue.new(permitted_params[:issue])
    @item.save
    render 'quick_response', layout: false
  end

  collection_action :calendar, method: :get do
    @project = nil
    if params[:project_id]
      @project = Project.find(params[:project_id])
    end

    @projects = current_user.projects

    render 'calendar'
  end

  collection_action :calendar_list, method: :get do

    @items = Issue

    if params[:project_id]
      @items = @items.where('project_id' => params[:project_id])
    else
      @items = @items.where(:project => { "$in" => current_user.projects.map{|x| x._id}} )
    end

    if params[:start]
      @items = @items.where('due_date' => { "$gte" => params[:start] })
    end

    if params[:end]
      @items = @items.where('due_date' => { "$lte" => params[:end] })
    end

    if params[:due_date]
      if params[:due_date] == 'false'
        @items = @items.where('due_date' => nil)
      else
        @items = @items.where('due_date' => params[:due_date])
      end
    end

    if @items == Issue
      render :json => []
    else
      @prepared_items = []
      @items = @items.order_by(:status => 'desc')
      for i in @items
        i['project_name'] = i.project.name
        i['overdue'] = i.is_over_due ? 'overdue' : ''
        @prepared_items << i
      end
      render :json => @prepared_items
    end
  end

  collection_action :calendar_update, method: :post do

    issue = Issue.find(params['id'])
    issue.due_date = params['due_date']
    issue.save()
    render :json => params
  end


  member_action :mark_as_complete do
    status = params['status']
    last_status = resource.status
    resource.status = status
    resource.save!
    render :js => "$('.flashes').html('<div class=\"flash flash_notice\">Mark issue as "+status+"!</div>'); $('#issue_"+resource.id+" .col-status').html(\"<span class='status_tag "+status+"'>"+status+"</span>\"); $('#issue_"+resource.id+"').removeClass(\""+last_status+"\"); $('#issue_"+resource.id+"').addClass(\""+status+"\")";
  end

  action_item :calendar, except: :calendar do
    link_to 'Calendar', calendar_admin_issues_path(), class:"btn-calendar"
  end

  action_item :calendar, only: [:calendar] do
    link_to 'All Issues', admin_issues_path(), class:"btn-calendar"
  end

  #index(as: :issue_index, :row_class => -> record { record.label_status }) do
  index(:row_class => -> record { record.label_status }) do
    sortable_handle_column
    selectable_column
    column :project
    column :id
    column :priority do |resource|
               status_tag resource.priority
    end
    column :componant do |resource|
      status_tag resource.componant
    end
    column :title
    column :due_date
    column :status do |resource|
      status_tag resource.status
    end
    if current_user.role != 'client'
      column :estimate_manhours
      column :actual_manhours
    end
    column "Assigned To", :user
    column :updated_at

    actions dropdown: true, defaults: false do |resource|
      if current_user.role == 'team'
        item( 'Queue', mark_as_complete_admin_issue_path(resource, { status:'queue'}), :remote => true)
        item( 'Doing', mark_as_complete_admin_issue_path(resource, { status:'doing'}), :remote => true)
        item( 'Testing', mark_as_complete_admin_issue_path(resource, { status:'testing'}), :remote => true)
      elsif current_user.role == 'client'
        item( 'Queue', mark_as_complete_admin_issue_path(resource, { status:'queue'}), :remote => true)
        item( 'Complete', mark_as_complete_admin_issue_path(resource, { status:'complete'}), :remote => true)
      else
        item( 'Queue', mark_as_complete_admin_issue_path(resource, { status:'queue'}), :remote => true)
        item( 'Doing', mark_as_complete_admin_issue_path(resource, { status:'doing'}), :remote => true)
        item( 'Testing', mark_as_complete_admin_issue_path(resource, { status:'testing'}), :remote => true)
        item( 'Complete', mark_as_complete_admin_issue_path(resource, { status:'complete'}), :remote => true)
      end
    end

    if current_user.role != 'client'
      actions do |resource|
        link_to 'Timesheet', admin_timesheets_path(:q => {:issue_id_eq => resource} )
      end
    end

  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs "Admin Details" do
      if !f.object.new_record?
        f.object.user ||= current_user
      end
      f.input :project, :as => :select2, :collection => current_user.projects, include_blank: false
      f.input :type, :as => :select2, :collection => Issue::TYPES, include_blank: false
      f.input :priority, :as => :select2, :collection => Issue::PRIORITY, include_blank: true
      f.input :componant, :as => :select2, include_blank: false, :input_html => {'data-option-dependent' => true, 'data-option-url' => ajax_filter_admin_componants_path(:format => 'json') + '?project_id=:issue_project_id&status=doing', 'data-option-observed' => 'issue_project_id'}, :collection => []
      f.input :title
      f.input :due_date, as: :datepicker, datepicker_options: { min_date: 3.days.ago.to_date }
      f.input :user, :label => "Assigned To"
      f.input :estimate_manhours
      f.input :status, :as => :select, :collection => Issue::STATUS, include_blank: false
      f.input :test_script
      f.input :robot_test_script
      f.input :description, as: :text, :wrapper_html => { :class => 'input-markdown' }
    end
    f.inputs "Attachments" do
      f.has_many :attachments, :heading => false do |p|
        p.input :attachment, :as => :file , :label => 'Select image', :image_preview => true
      end
    end
    f.actions
  end


end
