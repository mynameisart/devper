  ActiveAdmin.register Project do

  menu parent: "Projects"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  permit_params do
    permitted = [:name, :project_code, :user_ids => []]
    # permitted << :other if resource.something?
    permitted
  end

  index do
    selectable_column
    column :name
    column :project_code
    column :estimate_manhours
    column :issue_count
    column :actual_manhours
    actions dropdown: false do |project|
      link_to 'Timesheet', admin_timesheets_path(:q => {:project_id_eq => project} )
    end

  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs "Admin Details" do
      f.input :name
      f.input :project_code
      f.input :description
      f.input :git
      f.input :trello
      f.input :estimate_manhours
      f.input :users, as: :check_boxes
    end

    f.inputs do
      f.has_many :milestones, heading: 'Milestones', allow_destroy: true, new_record: true do |a|
        a.input :title
        a.input :description
        a.input :due_date, as: :datepicker , datepicker_options: { min_date: 3.days.ago.to_date, max_date: "+1W" }
      end
    end

    # f.inputs do
    #   f.has_many :issues, heading: 'Tasks / Issues', allow_destroy: true, new_record: true do |a|
    #     a.input :type, as: :select, collection: Issue::TYPES, include_blank: false
    #     a.input :title
    #     a.input :description
    #     a.input :priority
    #     a.input :due_date, as: :datepicker, datepicker_options: { min_date: Time.now.to_date }
    #   end
    # end

    f.actions
  end

    # action_item :create_timesheet do
    #
    #   link_to 'Create Timesheet', new_admin_timesheet_path(:timesheet => { :project_id => project })
    # end


end
