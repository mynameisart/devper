ActiveAdmin.register User do

  permit_params :username, :email, :password, :password_confirmation, :role, :project_ids => []

  index do
    selectable_column
    id_column
    column :username
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :username
  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs "Admin Details" do
      f.input :username
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :role, :as => :select, :collection => User::ROLES, include_blank: false
      f.input :projects, as: :check_boxes
    end
    f.actions
  end

end
