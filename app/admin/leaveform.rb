ActiveAdmin.register Leaveform do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  permit_params :user_id, :username, :type, :starts_at, :ends_at, :status, :id, :name, :title

  filter :user
  filter :type, as: :select, collection: Leaveform::TYPES
  filter :status, as: :select, collection: Leaveform::STATUS

  scope("Not Allow") { |scope| scope.where(:status =>  'not_allow') }
  scope("Allow") { |scope| scope.where(:status =>  'allow') }
  scope :all, default: true

  member_action :mark_as_complete do
    status = params['status']
    last_status = resource.status
    resource.status = status
    resource.save!
    render :js => "$('.flashes').html('<div class=\"flash flash_notice\">Mark leaveform as "+status+"!</div>');
                       $('#leaveform_"+resource.id+" .col-status').html(\"<span class='status_tag "+status+"'>"+status+"</span>\");
                           $('#leaveform_"+resource.id+"').removeClass(\""+last_status+"\"); $('#leaveform_"+resource.id+"').addClass(\""+status+"\")";
  end

  controller do
    def create
      super
      @leaveform.user = current_user
      @leaveform.save
    end
  end

  # after_create do |leaveform|
  #   leaveform.user = current_user
  #   leaveform.save
  # end


  # =========================================================================================================================

  index do
    if current_user.role == 'superadmin'
      # sortable_handle_column
      selectable_column
    end
    #id_column # column :id
    column :user
      # current_user.username
    column :type
    column :title
    column :status do |resource|
      status_tag resource.status
    end
    column :starts_at
    column :ends_at
    column :created_at
    if current_user.role == 'superadmin'
      actions dropdown: true, defaults: false do |resource|
        item( 'Allow', mark_as_complete_admin_leaveform_path(resource, { status:'allow'}), :remote => true)
        item( 'Not Allow', mark_as_complete_admin_leaveform_path(resource, { status:'not_allow'}), :remote => true)
      end
    end
    if current_user.role == 'superadmin'
     actions
    else
      column "Edit" do |leave|
        # puts "================== TEST"
        # puts "================== TEST #{leave.user.username}"
        # puts "================== TEST"
        if leave.user.username == current_user.username
          a href:"/admin/leaveforms/#{leave.id}/edit", class:"edit_link member_link" do
            "Edit"
          end
          # link_to 'Delete', admin_leaveform_path(leave), method: :delete, data: { confirm: 'Are you sure?' }
        end
       end
    end
  end

  form do |f|
    f.inputs "Leaveform Details" do
      if !f.object.new_record?
        f.object.user ||= current_user
      end
      # f.input :name, :input_html => { :value => current_user.username, disabled: true }
      # f.input :user, :input_html => { :value => current_user, disabled: true }
      f.input :title
      f.input :type, :as => :select2, :collection => Leaveform::TYPES, include_blank: true, :label => "Type of leave"
      f.input :starts_at, as: :datepicker, datepicker_options: { min_date: "+0D"} # :as => :time_picker
      f.input :ends_at,   as: :datepicker, datepicker_options: { min_date: "+0D"}
    end
    f.actions
  end

  show do |resource|
    attributes_table  do
      row "Title" do
        resource.title
      end
      row "Start Date" do
        resource.starts_at
      end
      row "End Date" do
        resource.ends_at
      end
    end
  end
end
