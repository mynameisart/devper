ActiveAdmin.register Knowledge do

  menu parent: "Knowledge Base"


  permit_params do
    permitted = [:title, :reflink, :body]
    # permitted << :other if resource.something?
    permitted
  end

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  index do
    selectable_column
    column :title
    # column :reflink
    column :updated_at

    actions

  end


  form do |f|

    f.semantic_errors *f.object.errors.keys

    f.inputs "Content" do
      f.input :title
      f.input :reflink, label: "Reference link"
      f.input :body, as: :text, :wrapper_html => { :class => 'input-markdown' }
      f.actions         # adds the 'Submit' and 'Cancel' buttons
    end
  end

end
