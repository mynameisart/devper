module ActiveAdmin
  module Views
    class HeaderRenderer
      def to_html
        title + custom_block + global_navigation + utility_navigation
      end

      def custom_block
        # Your custom block
      end
    end
  end
end